#Version 0.4
 * Possibilite de supprimer des entrees *Fait*
 * Parsing ? *Fini, pour la fonction afficher*


#Version 1.0
 * Lors de la suppression, si jour est vide, utiliser le jour actuel
 * Possibilite de modifier les entrees
 * Au moins un graphe
 * Objectifs
 * Parsing des arguments
 * Base de donnees dans un dossier exterieur au programme
 * Documentation et aide


#Plus tard

 * Graphes
 * Shell colore
 * Objectifs
 * Requetes avancees
 * Ajout d'un chronometre
