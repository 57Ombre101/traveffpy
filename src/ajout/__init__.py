"""
Module traitant l'ajout de données
"""

import time as t
from src.requete import lire_requete

def ajouter_valeur(connect):
    """Permet à l'utilisateur d'entrer une valeur à ajouter. c est un curseur reliant à la base"""
    c = connect.cursor()
    ## Entrée de l'utilisateur
    duree = input("Durée : ")
    efficacite = input("Efficacité : ")
    appreciation = input("Appréciation : ")
    matiere = input("Matière : ")
    commentaire = input("Commentaire : ")
    jour = input("Date : ")
    if jour == "":
        # Mettre le jour par défaut à aujourd'hui
        jour_t = t.gmtime()
        jour = t.strftime("%Y-%m-%d", jour_t)
    p = (duree,efficacite,appreciation,matiere,commentaire,\
jour)
    ##Enregistrement dans la base
    req = lire_requete("insertion-donnee")
    c.execute(req,p)
    annuler = input("Annuler ? ")
    if annuler == "non" or annuler == "n":
        connect.commit()
    else: # Par défaut, annule (prudence...)
        connect.rollback()
