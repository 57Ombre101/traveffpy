"""
Module permettant de faire ce qui est en relation avec la suppression de requete.
"""


from src.requete import requete
from src.affichage import afficher_jour_full


def suppr_id(identifier):
    """Supprime la donnee d'id identifier de la base."""
    p = (identifier,)
    requete(p, "supprimer-entree")


def is_identifier_valid(identifier):
    p = (identifier,)
    resultats = requete(p, "select-id")
    print(resultats)
    return len(resultats) == 1 and resultats[0][0] == identifier


def suppr_main():
    """Lien avec l'utilisateur"""
    jour = input("Jour : ")
    afficher_jour_full(jour)
    identifier = input("Id a supprimer : ")
    if is_identifier_valid(identifier):
        print("Saisie non valide")
    else:
        suppr_id(identifier)
        print("Donnée {} supprimée".format(identifier))
