"""
Module requète
"""

from src.sql import c

def lire_requete(fichier):
    """Prend le nom d'un fichier dans le dossier sql, sans \
extension, et retourn son contenu sous la forme d'une chaîn\
e de caractères."""
    with open("sql/" + fichier + ".sql", "r") as f:
        text = f.read()
    return text

def requete(p, fichier_requete):
    """Exécute la requète provenant d'un fichier

    p est un n-uplet de paramètres, fichier_requete est le \
fichier dans lequel est écrit la requète.
"""
    req = lire_requete(fichier_requete)
    c.execute(req, p)
    r = c.fetchall()
    return r
