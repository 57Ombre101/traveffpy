"""
Module shell
"""

import cmd
import argparse
import time as t
from src.affichage import afficher_semaine, afficher_jour
from src.ajout import ajouter_valeur
from src.ajout.supprimer import suppr_main


j_actuel = t.strftime("%Y-%m-%d", t.gmtime()) # jour actuel

class Shell(cmd.Cmd):
    intro = "Traveffpy\n—————————"
    prompt = "> "

    def __init__(self, connect):
        """c : curseur de connexion à la base sql"""
        cmd.Cmd.__init__(self)
        self.connect = connect
        self.c = connect.cursor()

    def do_ajouter(self, arg):
        """Ajoute une entrée à la base de données."""
        ajouter_valeur(self.connect)

    def do_afficher(self, arg):
        """Affiche une sélection

        affiche [-s <date>] : affiche le résumé de la semaine finissant par <date>
        affiche [<date>] : affiche la journée <date>

        Exemple :
    afficher : affiche le jour actuel
    afficher 2017-03-12 : affiche le jour 2017-03-12
    afficher -s : affiche la semaine actuelle
    afficher -s 2017-03-12 : affiche la semaine se terminant par 2017-03-12
        """
        args = parse_afficher(arg) #le tuple d'arguments
        if args[1] == True: # semaine spécifiée
            if args[0] != "":
                afficher_semaine(args[0])
            else:
                afficher_semaine()
        else:
            if args[0] != "":
                afficher_jour(args[0])
            else:
                afficher_jour()

    def do_supprimer(self, arg):
        """Supprime une entrée, par son id"""
        """D'abord, affine la recherche. Ensuite, l'utilisateur entre l'id a supprimer."""
        suppr_main()
        
    def do_quitter(self, arg):
        """Quitte l'application"""
        return True


def pre_parse(arg):
    return arg.split(" ")

def parse_afficher(args):
    """Utilisation :
    afficher : affiche le jour actuel
    afficher 2017-03-12 : affiche le jour 2017-03-12
    afficher -s : affiche la semaine actuelle
    afficher -s 2017-03-12 : affiche la semaine se terminant par 2017-03-12
    
    Donc, par défaut, jour = jour_actuel.
"""
    args = pre_parse(args)
    #print("Args :" + str(args))
    parser_afficher = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    
    parser_afficher.add_argument("jour", help = "Jour sélectionné", nargs='?', default = j_actuel)
    parser_afficher.add_argument("-s", "--semaine", help="Affiche la semaine", action = "store_true", default = False)
    args = parser_afficher.parse_args(args)
    if args.jour == "": # Cas intervenant lorsque
        # est entré "afficher", sans argument
        args.jour = j_actuel
    #print("Jour : " + str(args.jour))
    #print("Semaine : " + str(args.semaine))
    return (args.jour, args.semaine)
