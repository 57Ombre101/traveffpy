"""
Module pour l'affichage
"""

import time as t
from src.requete import requete

def get_jour_full(date):
    """ """
    p = (date,)
    r = requete(p, "select-jour-full")
    return r


def get_jour(date):
    """Retourne un tableau des différents temps d'une journée.

    date est sous la forme 'aaaa-mm-jj'. Le résultat est un tableau de couples (xxx, temps, efficacité, appréciation, matière, commentaire).
"""
    p = (date,) #p est un n-uplet, ici un 1-uplet
    r = requete(p, "select-jour")
    return r

def somme_jour(date):
    """Retourne la somme des temps de la journée date.

    date est 'aaaa-mm-jj'.
"""
    p = (date,)
    r = requete(p, "somme-jour")
    return r

def somme_eff_jour(date):
    """Retourne la somme pondérée des temps d'une journée

    Somme(temps * efficacité)
    date : str 'aaaa-mm-jj'
    s est un flottant
"""
    r = get_jour(date)
    s = 0
    for x in r:
        s += x[1] * x[2]/100 #division par 100 car 0 < eff < 100
    return s


def afficher_jour(date = t.strftime("%Y-%m-%d", t.gmtime())):
    s = somme_jour(date)
    r = get_jour(date)
    s_eff = somme_eff_jour(date)
    text = "Tps | Eff | Appréciation | Matière | Commentair\
e\n"
    for x in r:
        text = text + str(x[1]) + "  | " +  str(x[2]) + "  | " + x[3] + " | " + x[4] + " | " + x[5] + "\n"
    print("Durée totale : " + str(s[0][0]) + " minutes")
    print("Durée effective : " + str(int(s_eff)) + " minutes") #A finir (affichage entier)
    print(text)

def afficher_jour_full(date = t.strftime("%Y-%m-%d", t.gmtime())):
    entete = "Id | Tps | Eff | Appréciation | Matière | Commentaire"
    print(entete)
    for x in get_jour_full(date):
        txt = ""
        for k in range(len(x)-2):
            txt = txt + str(x[k]) + " | "
        txt = txt + str(x[len(x)-2])
        print(txt)

def afficher_semaine(fin_txt=""):
    """ """
    if fin_txt == "":
        actuel = t.gmtime()
        fin_txt = t.strftime("%Y-%m-%d", actuel)
    #else:
    #fin_tuple = t.strptime(fin_txt, "%Y-%m-%d")

    fin_tuple = t.strptime(fin_txt, "%Y-%m-%d")
    fin_s = t.mktime(fin_tuple)
    #creation de la variable debut
    duree_s = 6 * 24 * 3600 #nombre de secondes dans une semaine
    debut_s = fin_s - duree_s
    debut_tuple = t.gmtime(debut_s)
    debut_txt = t.strftime("%Y-%m-%d", debut_tuple)
    p = (debut_txt, fin_txt)
    r = requete(p, "select-plage-resume")
    print("debut : " + debut_txt)
    print("fin : " + fin_txt)
    print("Date | S_tps | S_pond | Nbr | S_eff")
    for x in r:
        print(x[0] +" | "+ str(x[1]) +" | "+ str(x[2]) +" | "+ str(x[3]) +" | "+ str(x[4]) )
