CREATE TABLE temps (
       id INTEGER,
       duree INTEGER,
       efficacite INTEGER,
       appreciation VARCHAR(200),
       matiere VARCHAR(50),
       commentaire VARCHAR(200),
       jour DATE,
       PRIMARY KEY(id)
);
