SELECT jour, SUM(duree), SUM(duree * efficacite /100), COUNT(*), SUM(efficacite)/COUNT(*)
       FROM temps
       WHERE jour >= date(?) AND jour <= date(?)
       GROUP BY jour
;
