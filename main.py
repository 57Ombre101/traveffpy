
##Initialisation
from src.aide import afficher_aide
from src.shell import Shell
from src.sql import connect

###Pour le futur :
#import matplotlib.pyplot as mp


###Préparation de l'interface
s = Shell(connect)

##Boucle principale
###Lancement du shell (processus principal)
s.cmdloop() #tout est caché dedans


##Tout doit être fermé !
###Déconnection à la base
connect.close()
