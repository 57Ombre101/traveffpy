all : readme todo

readme : README
	~/scripts/mopymark.py README readme.html

todo : todo.md
	~/scripts/mopymark.py todo.md todo.html
